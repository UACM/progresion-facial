# Progresion facial

Propuesta de investigación seleccionada en la Convocatoria 2022-Proyectos de investigación en Inteligencia Artificial en el Espacio de Innovación UNAM-HUAWEI.

Somos un equipo interdisciplinario que busca atender el problema de la desaparición forzada de menores en México, a partir del desarrollo de software que realice progresiones de edad implementando DL-Inteligencia Artificial.

Buscamos contribuir a los colectivos de búsqueda de menores y a las autoridades encargadas de la procuración de justicia, a través de la generación de imágenes de la posible apariencia del rostro de la persona, a pesar del tiempo que haya pasado desde su desaparición.



### Instalación

Sigue la guía de `install.md`. Por ahora sólo está escrito cómo usar e instalar `DVC` el manejador de links.


## Unos bookmarks

 - Muestra prompts e imágenes generadas con stable diff: <https://playgroundai.com/>

 - Synergy between 3DMM and 3D Landmarks for Accurate 3D Facial Geometry <https://github.com/choyingw/SynergyNet>

 - Here are 34 public repositories about 3d face reconstruction <https://github.com/topics/3d-face-reconstruction>

 - Dos tutoriales a cargo de DotCSV <https://www.youtube.com/watch?v=hZNrlHmA4WQ> <https://yewtu.be/watch?v=rgKBjRLvjLs>

 - Tutorial stable diffusion from scratch: <https://www.youtube.com/watch?v=TBCRlnwJtZU>

 - Dataset de rostros y edades: <https://www.kaggle.com/datasets/jangedoo/utkface-new>

 - Recursos de la Alianza: <https://alianza.unam.mx/recursos/>

----


<https://www.sciencedirect.com/science/article/pii/S1319157820304821#b0040>

<https://github.com/1adrianb/face-alignment>

<https://github.com/google/mediapipe/wiki/MediaPipe-Face-Mesh>

[Learning shape and texture progression for young child face aging](https://doi.org/10.1016/j.image.2020.116127)

<https://github.com/yuval-alaluf/hyperstyle>

https://paperswithcode.com/paper/image2stylegan-how-to-embed-images-into-the
