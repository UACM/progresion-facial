import face_alignment
from skimage import io
import collections
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


fa = face_alignment.FaceAlignment(face_alignment.LandmarksType._3D, device='cpu', flip_input=False)

input = io.imread('data/FGNET/images/041A00.JPG')
preds = fa.get_landmarks(input)


pred_type = collections.namedtuple('prediction_type', ['slice', 'color'])
pred_types = {'face': pred_type(slice(0, 17), (0.682, 0.780, 0.909, 0.5)),
              'eyebrow1': pred_type(slice(17, 22), (1.0, 0.498, 0.055, 0.4)),
              'eyebrow2': pred_type(slice(22, 27), (1.0, 0.498, 0.055, 0.4)),
              'nose': pred_type(slice(27, 31), (0.345, 0.239, 0.443, 0.4)),
              'nostril': pred_type(slice(31, 36), (0.345, 0.239, 0.443, 0.4)),
              'eye1': pred_type(slice(36, 42), (0.596, 0.875, 0.541, 0.3)),
              'eye2': pred_type(slice(42, 48), (0.596, 0.875, 0.541, 0.3)),
              'lips': pred_type(slice(48, 60), (0.596, 0.875, 0.541, 0.3)),
              'teeth': pred_type(slice(60, 68), (0.596, 0.875, 0.541, 0.4))
              }


fig = plt.figure(figsize=plt.figaspect(.5))

plot_style = dict(marker='o',
                  markersize=4,
                  linestyle='-',
                  lw=2)

ax = fig.add_subplot(1, 2, 1)

ax.imshow(input)

for pred_type in pred_types.values():
    ax.plot(preds[0][pred_type.slice,0],
            preds[0][pred_type.slice,1],            
            color=pred_type.color, **plot_style)

ax.axis('off')

# 3D-Plot
ax = fig.add_subplot(1, 2, 2, projection='3d')

surf = ax.scatter(preds[0][:, 0] * 1.2,
                  preds[0][:, 1],
                  preds[0][:, 2],
                  c='cyan',
                  alpha=1.0,
                  edgecolor='b')

for pred_type in pred_types.values():
    ax.plot3D(preds[0][pred_type.slice, 0] * 1.2,
              preds[0][pred_type.slice, 1],
              preds[0][pred_type.slice, 2], color='blue')

ax.view_init(elev=0., azim=80.)
ax.set_xlim(ax.get_xlim()[::-1])
plt.show()
