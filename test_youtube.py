from caras_youtube.video_metadata import Metadata

metadata = Metadata()

id = 'thelumberjay_jeremy_takes_a_photo_of_himself_everyday_for_12_years'


metadata.add_new_video_info(
    url='https://www.youtube.com/watch?v=750ObM_lyOI',
    id=id,
    start_age=12,
    end_age=24,
    start='0:09',
    end = '3:49'
)

metadata.download_video(id)
metadata.calculate_video_metadata(id)
metadata.video2frames(id)

print(metadata[id])



