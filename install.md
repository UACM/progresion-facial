# Guia para usar DVC

DVC —_Data Version Control_— es una herramienta de Ciencia de datos que se utiliza para llevar un registro ordenado y simplificar las descargas y subidas de los datos a una base de datos. En nuestro caso, se usa para generar una conexión con Next Cloud.

Hay que mencionarque DVC funciona integralmente con Git, por lo que se requiere tener el repositorio del clonado proyecto de forma local, y Git.



## Instalación

Dependiendo del sistema operativo se puede usar esta [guia](https://dvc.org/doc/install). Sin embargo, la manera más fácil de instalarlo es como librería de Python:

### Usando _Mamba_

Se puede instalar con [Mamba](https://mamba.readthedocs.io/en/latest/installation.html)—Mamba es una manejador de paquetes de Python.

Se recomienda instalarlo en el ambiente virtual que se usa para el proyecto.


```sh
mamba install -c conda-forge dvc
```

### Usando _pip_

```
pip install dvc
```

Tambien se debe instalar una libería especial para el manejo de _WebDav_ que usa NextCloud para crear conexión con DVC.

```sh
mamba install -c conda-forge 'dvc[webdav]'
```
O:

```sh
pip install -c conda-forge 'dvc[webdav]'
```

## Contraseña

Ahora, estando en el directorio base del proyecto, hay que añadir la contraseña para conectar, esta se puede solicitar en el grupo de Telegram o con rodoart`arroba`ciencias.unam.mx.

```sh
dvc remote modify --local nextcloud_rodoart password INSERTAR_CONTRASEÑA_AQUÍ
```


## Descargar archivos

Ya esta listo. Desde el directorio raíz podrías descargar cualquiera de los archivos con extensión `.dvc` del proyecto. Con el comando `dvc pull`  y la dirección relativa, por ejemplo:

```sh
dvc pull data/raw/test.txt.dvc
```

debería descargar el archivo test.txt en el directorio `data/raw/`

## Subir archivos

Para subir archivos directamente a NextCloud, simplemente se usa el comando `dvc add` y la dirección relativa. Por ejemplo

```sh
dvc add data/hola_mundo.jpg
```
Pedirá añadir dos al stage de Git, simplemente usa:

```sh
git add -A
```

Y luego sube el archivo con  

```sh
dvc push
```
¡Ahora cuando hagas un commit y sincronizes con un push en git, todos podremos descargarlo fácilmente!.

## Añadir URL

Si tienes otro origen de datos de la web, puede añadirlo usando

```
dvc import-url http://tu.direccion.web directorio/relativo/nombre_del_archivo
```

Automáticamente se descargará. y se añadirá al caché de DVC. Deberás añadir los nuevos archivos creados a Git con

```
git add -A
```

y subir el archivo al almacenamiento de Next Cloud con.

```
dvc push
```

