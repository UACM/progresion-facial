from pytube import YouTube
from pathlib import Path
from os import makedirs



def download(
        link:str,
        output_dir: Path,
) -> str:
    """Download a youtube video in high quality.

    Args:
        link: The url of the video.

        output_dir: Download directory.

    """  

    
    def _complete_callback(stream, file_path):
        print(f'The file has been successfully downloaded to the folder:\n'+
            f'{file_path}')
        
        file_path = Path(file_path).resolve
        return  file_path
    
    
    # Check if it is with the correct variable type, otherwise it is converted.
    if output_dir is not Path:
        output_dir = Path(output_dir).resolve()


    # Check if the output dir exists
    makedirs(output_dir, exist_ok=True)


    youtubeObject = YouTube(
        link,
        on_complete_callback=_complete_callback,
    )
    youtubeObject = youtubeObject.streams.get_highest_resolution()
    
    return youtubeObject.download(output_path=str(output_dir))



def _progress_callback(stream, data_chunk, bytes_remaining):
    pass
