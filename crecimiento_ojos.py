from pathlib import Path
import face_alignment
from skimage import io
import numpy as np

fa = face_alignment.FaceAlignment(face_alignment.LandmarksType._3D,
                                  device='cpu',
                                  flip_input=False)


pics = ['001A02.JPG',
        '001A05.JPG',
        '001A08.JPG',
        '001A10.JPG',
        '001A14.JPG',
        '001A16.JPG',
        '001A18.JPG',
        '001A19.JPG',
        '001A22.JPG',
        '001A28.JPG',
        '001A29.JPG',
        '001A33.JPG',
        '001A40.JPG',
        '001A43a.JPG',
        '001A43b.JPG',]

pred_types = {'face': slice(0, 17),
              'eyebrow1': slice(17, 22),
              'eyebrow2': slice(22, 27),
              'nose': slice(27, 31),
              'nostril': slice(31, 36),
              'eye1': slice(36, 42),
              'eye2': slice(42, 48),
              'lips': slice(48, 60),
              'teeth': slice(60, 68),
              }

def centroid(img, preds, landmark):
    return np.array([np.mean(preds[0][pred_types[landmark], 0])/img.shape[0],
                     np.mean(preds[0][pred_types[landmark], 1])/img.shape[1]])


def inter_eye_distance(img, preds):
    return (centroid(img, preds, 'eye2') - centroid(img, preds, 'eye1'))


for p in pics:
    path = Path('data/FGNET/images/')
    img = io.imread(path.joinpath(p))
    print(p, img.shape, inter_eye_distance(img, fa.get_landmarks(img)))
