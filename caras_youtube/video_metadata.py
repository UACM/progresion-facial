import json
from pathlib import Path
from os import makedirs, rename
from warnings import warn
from numpy import linspace
import cv2 as cv


from .youtube_downloader import download

from typing import Optional, Union

JSON_FILE_PATH = Path('data/interim/youtube_videos/metadata.json').resolve()
VIDEO_DIR_PATH = Path('data/raw/youtube_videos').resolve()
IMAGES_DIR_PATH = Path('data/processed/youtube_videos').resolve()


NUMBER_OF_PICTURES_PER_YEAR = 5

_EXAMPLE_DICT = {
    'example_id':{
        'name':'Photo A Day For 10 Years (Age 13 to 23)',
        'url': 'https://www.youtube.com/watch?v=zuRd_Eneuk8',
        'start_age': 13,
        'end_age': 23,
        'start': '0:00',
        'end': '4:20',
        'duration':261,
        'number_of_frames':7819,
        'fps':29.97,
        'file_name':'example',
        'status':'example'
    }
}


class Metadata():
    """Handles an internal dictionary of video metadata in a json file.

    Attributes:

        json_path: Location where you want to generate and load the json file. 
        By default it is: data/interim/youtube_videos/metadata.json.


    Methods:

    """
    def __init__(
        self, 
        json_path:Optional[Union[Path,str]] = JSON_FILE_PATH,
        video_path:Optional[Union[Path,str]] = VIDEO_DIR_PATH,
        images_path:Optional[Union[Path,str]]=IMAGES_DIR_PATH
    )-> None:
        
        # Json Path attribute.   
        if not type(json_path) == Path:
            json_path = Path(json_path).resolve()
             
        self.json_path = json_path
        
        # Video Path attribute. 
        if not type(video_path) == Path:
            video_path = Path(video_path).resolve()
             
        self.video_path = video_path

        # Images Path attribute. 
        if not type(images_path) == Path:
            images_path = Path(images_path).resolve()
             
        self.images_path = images_path

        self.dictionary = {}
        
        # Create if not exists.
        self._create_metadata_file()

        # Load metadata to self dictionary.
        self._load_metadata()

    def __str__(self) -> str:
        return str(self.dictionary)
    
    def __repr__(self) -> str:
        return str(self.dictionary)

    def __iadd__(self, other:dict[dict]):
        self._save_metadata(other)

    def __getitem__(self, id:str)->dict:
        if id in self.dictionary.keys():
            return self.dictionary[id]
        else:
            raise AssertionError(f"Id = {id} doesn't exist")


    def add_new_video_info(self, url, id, start_age, end_age, **kwargs):
        new_dictionary =  {
        id:{
            'name':'Photo A Day For 10 Years (Age 13 to 23)',
            'url': url,
            'start_age': start_age,
            'end_age': end_age,
            'status':'new'
            }
        }

        if not len(kwargs)==0:
            new_dictionary[id] = new_dictionary[id] | kwargs
        
        self += new_dictionary

    def download_video(self, id):
        output_file_path = self.video_path.joinpath(f'{id}.mp4')

        if output_file_path.is_file():
            warn(
                f'{output_file_path} already exists, skipping the download...',
                Warning,
                stacklevel=2
            )

            if not self[id]['status'] \
               in ['downloaded', 'calculated_metadata', 'finished']:
                self[id]['status'] = 'downloaded'
                self._update_metadata()
            
            return
    
        file_path = download(link=self[id]['url'],output_dir=self.video_path)
        file_path = Path(file_path).resolve()       
        rename(file_path, output_file_path)

        self[id]['status'] = 'downloaded'
        self._update_metadata()

    def calculate_video_metadata(self, id):
        
        video_file_path = self.video_path.joinpath(f'{id}.mp4')
        cap = cv.VideoCapture(str(video_file_path))

        fps = cap.get(cv.CAP_PROP_FPS)      
        frame_count = int(cap.get(cv.CAP_PROP_FRAME_COUNT))
        duration = frame_count/fps

        new_dict = {
            'duration': duration,
            'number_of_frames': frame_count,
            'fps':fps
        }

        if not 'start' in self[id].keys():
            start = 0
            new_dict['start'] = start
        
        elif type(self[id]['start'])==str: 
            self[id]['start'] = to_seconds(self[id]['start'])
        
        else:
            start = self[id]['start']


        if not 'end' in self[id].keys():
            end = int(duration)
            new_dict['end'] = end
        
        elif type(self[id]['end'])==str: 
            self[id]['end'] = to_seconds(self[id]['end'])

        else:
            end = self[id]['end']         

        num = (self[id]['end_age']-self[id]['start_age'])\
               *NUMBER_OF_PICTURES_PER_YEAR

        new_dict['num'] = num

        self[id]['status'] = 'calculated_metadata'
        self.dictionary[id] = self[id] | new_dict
        self._update_metadata()

        cap.release()   

    def video2frames(self, id): 

        output_path = self.images_path.joinpath(id)
        video_path = self.video_path.joinpath(f'{id}.mp4')

        if not output_path.is_dir():
            makedirs(output_path)


        ages = linspace(start=self[id]['start_age'], 
                        stop=self[id]['end_age'],
                        num=self[id]['num'])
        


        frames = linspace(start=self[id]['start']*self[id]['fps'],
                          stop=self[id]['end']*self[id]['fps'],
                          num=self[id]['num'])

        ages = [round(k,1) for k in ages]
        frames = [round(k) for k in frames]
           
        cap = cv.VideoCapture(str(video_path))


        frame = 0        
        index = 0
        while cap.isOpened():
            Ret, Mat = cap.read()
            if Ret:
                frame += 1
                if frame in frames:              
                    cv.imwrite(
                        str(output_path.joinpath(f'{ages[index]}.png')), Mat
                    )
                    index+=1
                
            else:
                break
        cap.release()
        self.dictionary[id]['status'] = 'finished'

        return       


        
    def _create_metadata_file(self) -> None:        
        if  self.json_path.is_file():
            return
        
        makedirs(self.json_path.parent, exist_ok=True)

        with open(self.json_path, 'w') as outfile:
            json.dump(_EXAMPLE_DICT, outfile, indent=4)


    def _load_metadata(self)->dict:
        
        self._create_metadata_file()

        with open(self.json_path) as json_file:
            self.dictionary = json.load(json_file)
            return self.dictionary
    

    def _save_metadata(self, to_add_dictionary:dict[dict])->None:
        
        new_dictionary = self.dictionary|to_add_dictionary
        self.dictionary = new_dictionary

        self._update_metadata()
        
        

    def _update_metadata(self):
        with open(self.json_path, 'w') as outfile:
            json.dump(self.dictionary, outfile, indent=4)

        
    def _add_dict_to_entry(self,id:str, other:dict):
        new_dict = {id:other}

        self += new_dict




def to_seconds(timestr):
    seconds= 0
    for part in timestr.split(':'):
        seconds= seconds*60 + int(part, 10)
    return seconds




